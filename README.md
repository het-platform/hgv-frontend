# hgv-frontend

This repository contains the code for the Harry Go Vert crossplatform app. We use Ionic to build the [Harry Go Vert](https://ionicframework.com/) app for Android, iOS and Web platforms. We also use [Capacitor](https://capacitorjs.com/docs), a crossplatform native runtime, and [Angular](https://angular.io/), a framework for building modern web applications.

## Table of contents

- [Tools to install](#tools-to-install)
- [Git](#git)
- [Pre-commit](#pre-commit)
- [Running the app](#running-the-app)
- [Performance](#performance)
- [Deploying to production (web)](#deploying-to-production-web)
- [Troubleshooting and FAQ](#troubleshooting-and-faq)

## Tools to install

To get your system ready for development, follow the following steps:

1. Install `node` version 14 or 16. This will also install `npm`.
1. Install the Ionic CLI globally: `npm install -g @ionic/cli`. Run `npm uninstall -g ionic` if you already have an older version of the Ionic CLI installed globally.
1. Install the npm dependencies by running `npm install`.
1. Install Android Studio.
1. Install XCode if you are on an Apple system.
1. Open the project folder in the text editor of your choice.

## Git

When you start making changes to the code, always follow these steps:

1. Check out the `develop` branch: `git checkout develop`.
1. Create a new branch to store your work on: `git checkout -b <prefix>/<your-branch-name>`.
1. When you are done making changes, run `git add .` and `git commit -m "<your descriptive commit message>"`.
1. When you are done making changes, run `git push -u origin <prefix>/<your-branch-name>`.
1. Create a merge request in GitLab to merge your new branch into develop.
1. (optional) Ask someone to review your merge request and complete it when it is approved.

We use the following branch prefixes:

- feat: a new feature.
- fix: fixing an issue.
- docs: updating documentation.
- refactor: refactoring existing code.
- perf: fixing a performance issue.
- revert: revert an earlier change.

## Pre-commit

We use an npm package called husky to run a script every `git commit` is executed. See the `.husky` folder for more info.

## Running the app

### Web

To serve the web version of the app, run `ionic serve`.

## Android

To run the app on an Android device (emulator or physical via usb):

1. Follow the Ionic Android Development setup guide: https://ionicframework.com/docs/developing/android
1. Build the Ionic app for Android and open Android Studio: `npx cap copy && npx cap sync && ionic cap build android`

### iOS

To run the app on an iOS device (emulator or physical via usb):

1. Follow the Ionic iOS Development setup guide: https://ionicframework.com/docs/developing/ios
1. Build the Ionic app for iOS and open Xcode: `npx cap copy && npx cap sync && ionic cap build ios`

## Formatting and linting

We use EditorConfig, ESlint and Prettier to make sure our code is formatted correctly on all developer devices. Since this can be a complicated topic, here are some guidelines:

- All configuration related to the editor (end of line, indent style, indent size...) should be handled by EditorConfig
- Everything related to code formatting should be handled by Prettier
- The rest (code quality) should be handled by ESLint

Read more [here](https://blog.theodo.com/2019/08/empower-your-dev-environment-with-eslint-prettier-and-editorconfig-with-no-conflicts/).

## Performance

To analyse the sizes of dependencies we use, we use source-map-explorer. Change `sourceMap` and `namedChunks` to `true` in angular.json and run `npm run source-map-explorer` to open an overview of the dependency sizes.

## Troubleshooting and FAQ

Below you can find a list of common problems and questions

### I can't open the iOS app.

- iOS development can only be done on systems with the MacOS operating system (for example MacBooks).

### I am getting errors in Android Studio

- Android Studio caches some build- or project files, which can cause problems. When errors appear in Android Studio, always run `File > Invalidate Caches / Restart > Invalidate and Restart` first.

- When you have problems with the automatic Gradle build, try opening a command line in the android folder and running `.\gradlew clean && .\gradlew app:bundleRelease`. The last command will show more details about any build problems.

- For proper functionality of Android Studio, make sure that the Android SDK Build Tools and Platform tools are installed in the Android SDK manager, you can open this from within Android Studio.

### The barcode scanner doesn't work

- The barcode-scanner we currently use places the CameraView below the WebView in the ViewStack, so to show the camera preview in the scanner we hide the body in the WebView.

Feel free to add problems here!
