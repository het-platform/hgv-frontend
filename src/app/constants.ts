/* eslint-disable @typescript-eslint/naming-convention */
// TODO: update eslint rules for naming convention

export const STORAGE_KEYS = {
  LAST_SCANNED: 'lastScannedProduct',
  CURRENT_CART: 'currentCart',
  SAVED_CARTS: 'savedCarts',
  USER_DATA: 'userData',
};

export const exampleBarcodes = ['3017620422003', '5449000000996', '3175680011480', '3046920022651', '3168930010265'];

export const FEEDBACK_URL =
  'https://docs.google.com/forms/d/e/1FAIpQLSeMdrwhc5YXVcflRPO8_V5xN3U-Aqy3PnFqs05cjesacGVVMQ/viewform';
