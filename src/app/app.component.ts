import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Store } from '@ngrx/store';
import { getCurrentCart } from './state/current-cart/current-cart.actions';

@Component({
  selector: 'hgv-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(private store: Store, private platform: Platform) {
    store.dispatch(getCurrentCart());
  }
}
