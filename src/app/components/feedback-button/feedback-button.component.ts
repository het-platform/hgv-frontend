import { Component } from '@angular/core';
import { Clipboard } from '@capacitor/clipboard';
import { ToastController } from '@ionic/angular';
import { FEEDBACK_URL } from 'src/app/constants';

@Component({
  selector: 'hgv-feedback-button',
  templateUrl: './feedback-button.component.html',
  styleUrls: ['./feedback-button.component.scss'],
})
export class FeedbackButtonComponent {
  constructor(private toastController: ToastController) {}

  async onFeedbackButtonClicked() {
    await Clipboard.write({
      // eslint-disable-next-line id-blacklist
      string: FEEDBACK_URL,
    });
    const toast = await this.toastController.create({
      message: 'Feedback link is gekopieerd.',
      duration: 3000,
    });
    await toast.present();
  }
}
