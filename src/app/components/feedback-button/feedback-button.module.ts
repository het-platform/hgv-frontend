import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { FeedbackButtonComponent } from './feedback-button.component';

@NgModule({
  declarations: [FeedbackButtonComponent],
  imports: [IonicModule],
  exports: [FeedbackButtonComponent],
})
export class FeedbackButtonModule {}
