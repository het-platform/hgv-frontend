import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CartItemComponent } from './cart-item.component';

@NgModule({
  declarations: [CartItemComponent],
  imports: [IonicModule],
  exports: [CartItemComponent],
})
export class CartItemModule {}
