import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CartItem } from 'src/app/state/app.models';

@Component({
  selector: 'hgv-cart-item',
  templateUrl: './cart-item.component.html',
  styleUrls: ['./cart-item.component.scss'],
})
export class CartItemComponent {
  @Input() item: CartItem;
  @Output() remove = new EventEmitter<string>();
  @Output() add = new EventEmitter<string>();
}
