/* eslint-disable @ngrx/avoid-mapping-selectors */
import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { selectCurrentCart } from 'src/app/state/current-cart/current-cart.selectors';

@Component({
  selector: 'hgv-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss'],
})
export class TabsComponent {
  currentCartItemCount: number;

  constructor(private store: Store) {
    // TODO: fix this somehow
    // eslint-disable-next-line @ngrx/avoid-mapping-selectors, @ngrx/no-store-subscription
    store.select(selectCurrentCart).subscribe((cart) => {
      this.currentCartItemCount = cart.items.reduce((sum, next) => (sum += next.quantity), 0);
    });
  }
}
