import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { SearchResultComponent } from './search-result.component';

@NgModule({
  declarations: [SearchResultComponent],
  imports: [CommonModule, IonicModule, RouterModule],
  exports: [SearchResultComponent],
})
export class SearchResultModule {}
