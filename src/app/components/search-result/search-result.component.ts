import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Product } from 'src/app/state/app.models';

@Component({
  selector: 'hgv-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.scss'],
})
export class SearchResultComponent {
  @Input() result: Product;
  @Input() editable: boolean;
  @Output() addToCart = new EventEmitter<Product>();
}
