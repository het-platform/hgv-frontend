import { AfterViewInit, Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { LoadingService } from 'src/app/services/loading.service';

/**
 * This component handles selecting an image file from the file system and showing the image in the interface.
 * It will also pass the selected file data to any parent event listener.
 */
@Component({
  selector: 'hgv-image-upload',
  templateUrl: './image-upload.component.html',
  styleUrls: ['./image-upload.component.scss'],
})
export class ImageUploadComponent implements AfterViewInit {
  @Input() uploadLabel: string;

  @Input() imageUrl: string;

  @Output() setImage = new EventEmitter<File>();

  @ViewChild('imagePreview') imagePreview: ElementRef;

  selectedInputFile: File = null;

  constructor(private loadingService: LoadingService) {}

  ngAfterViewInit() {
    console.log(this.imageUrl);
    setTimeout(async () => {
      if (this.imageUrl?.length > 0) {
        await this.loadingService.present();
        const response = await fetch(this.imageUrl, { mode: 'no-cors' });
        const data = await response.blob();
        const file = new File([data], new Date().getTime() + '.jpg');
        this.selectedInputFile = file;
        this.setImage.emit(this.selectedInputFile);
        setTimeout(async () => {
          this.imagePreview.nativeElement.src = this.imageUrl;
          await this.loadingService.dismiss();
        });
      }
    }, 500);
  }

  async selectImage(event: Event) {
    await this.loadingService.present();
    this.selectedInputFile = (event.target as HTMLInputElement).files[0];
    const fileReader = new FileReader();
    fileReader.onload = () => (this.imagePreview.nativeElement.src = fileReader.result as string);
    fileReader.onerror = () => console.error(fileReader.error);
    fileReader.readAsDataURL(this.selectedInputFile);
    this.setImage.emit(this.selectedInputFile);
    await this.loadingService.dismiss();
  }

  clearImage() {
    this.selectedInputFile = null;
    this.imagePreview.nativeElement.src = '';
  }
}
