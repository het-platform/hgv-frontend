import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { SavedCartComponent } from './saved-cart.component';

@NgModule({
  declarations: [SavedCartComponent],
  imports: [CommonModule, IonicModule, RouterModule],
  exports: [SavedCartComponent],
})
export class SavedCartModule {}
