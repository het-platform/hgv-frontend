import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Cart } from 'src/app/state/app.models';

@Component({
  selector: 'hgv-saved-cart',
  templateUrl: './saved-cart.component.html',
  styleUrls: ['./saved-cart.component.scss'],
})
export class SavedCartComponent {
  @Input() cart: Cart;
  @Output() edit = new EventEmitter<string>();
  @Output() remove = new EventEmitter<string>();
}
