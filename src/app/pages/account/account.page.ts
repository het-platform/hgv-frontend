import { Component } from '@angular/core';
import { DeviceService } from 'src/app/services/device.service';

@Component({
  selector: 'hgv-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})
export class AccountPage {
  constructor(public device: DeviceService) {}
}
