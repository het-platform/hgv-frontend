import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AccountPageRoutingModule } from './account-routing.module';

import { FeedbackButtonModule } from 'src/app/components/feedback-button/feedback-button.module';
import { AccountPage } from './account.page';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, AccountPageRoutingModule, FeedbackButtonModule],
  declarations: [AccountPage],
})
export class AccountPageModule {}
