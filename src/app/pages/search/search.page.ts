import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { BarcodeScanner } from '@capacitor-community/barcode-scanner';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { exampleBarcodes } from 'src/app/constants';
import { DeviceService } from 'src/app/services/device.service';
import { HttpService } from 'src/app/services/http.service';
import { LoadingService } from 'src/app/services/loading.service';
import { Product } from 'src/app/state/app.models';
import { addProductToCart } from 'src/app/state/current-cart/current-cart.actions';

// eslint-disable-next-line @typescript-eslint/naming-convention
declare const BarcodeDetector: any;

@Component({
  selector: 'hgv-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('videoObject') videoObject: ElementRef;

  searchTerm: string;
  scanning: boolean;
  productNotFound: boolean;
  error: Error | null;
  searchResults$: Observable<Product[]>;
  scannedProduct$: Observable<Product>;

  private scannedBarcode: string;

  // Web
  private barcodeDetector;
  private scanInterval;

  // Native
  private cameraPermissionGranted = false;
  private scanResult: any;

  constructor(
    private store: Store,
    private http: HttpService,
    private router: Router,
    private loadingService: LoadingService,
    public device: DeviceService
  ) {}

  async ngOnInit() {
    this.initializeSearch();
  }

  async ngAfterViewInit() {
    if (this.device?.isIphone) await BarcodeScanner.prepare();
  }

  async ngOnDestroy() {
    this.stopScanning();
  }

  ionViewDidEnter() {
    this.initializeSearch();
    document.addEventListener('keydown', (event: KeyboardEvent) => {
      if (event.key === ']') this.simulateProductScannedSucceeded(event);
      if (event.key === '[') this.simulateProductScannedFailed(event);
    });
  }

  ionViewDidLeave() {
    document.removeAllListeners('keydown');
  }

  initializeSearch() {
    this.searchTerm = '';
    this.scanning = false;
    this.productNotFound = false;
    this.error = null;
    this.searchResults$ = null;
    this.scannedProduct$ = null;
    this.scannedBarcode = null;
    this.scanResult = null;
    clearInterval(this.scanInterval);
  }

  async searchProductsByName() {
    if (this.searchTerm.length < 3) return;

    await this.loadingService.present();

    this.searchResults$ = this.http.getProductsByName(this.searchTerm).pipe(
      catchError(() => of([])),
      tap(async (results) => {
        console.log(results);
        await this.loadingService.dismiss();
      })
    );
  }

  changeSearch(): void {
    if (this.searchTerm?.length < 3) {
      this.searchResults$ = of([]);
      return;
    }
  }

  async startScanning() {
    console.log('Start scanning');

    this.initializeSearch();
    this.scanning = true;

    if (this.device?.isMobileWeb) {
      this.startScanningWeb();
    } else {
      await this.startScanningNative();
    }
  }

  async stopScanning() {
    console.log('Stop scanning');
    this.scanning = false;
    if (this.device?.isMobileWeb) {
      this.stopScanningWeb();
    } else {
      await this.stopScanningNative();
    }
  }

  addToCart(product: Product) {
    this.store.dispatch(addProductToCart({ product }));
  }

  submitNewProduct() {
    this.router.navigateByUrl(`submit/${this.scannedBarcode}`);
  }

  private async getCameraPermissionsNative() {
    console.log('Get camera permissions');
    const { granted } = await BarcodeScanner.checkPermission({ force: true });
    this.cameraPermissionGranted = granted;
  }

  private async startScanningNative() {
    console.log('Start scanning native');

    if (!this.cameraPermissionGranted) await this.getCameraPermissionsNative();

    if (this.cameraPermissionGranted) {
      document.querySelector('.hgv').classList.add('scanner-active');
      this.scanResult = await BarcodeScanner.startScan();

      if (this.scanResult.hasContent) {
        this.scannedBarcode = this.scanResult.content;
        this.scanning = false;
        await this.loadingService.present();
        this.scannedProduct$ = this.http.getProductByBarcode(this.scanResult.content).pipe(
          catchError(async () => {
            this.productNotFound = true;
            await this.stopScanningNative();
            await this.loadingService.dismiss();
            return undefined;
          }),
          tap(async () => {
            await this.stopScanningNative();
            await this.loadingService.dismiss();
          })
        );
      }
    } else {
      await this.stopScanningNative();
      await this.loadingService.dismiss();
    }
  }

  private async stopScanningNative() {
    console.log('Stop scanning native');
    await BarcodeScanner.stopScan();
    this.scanResult = undefined;
    this.scanning = false;
    document.querySelector('.hgv').classList.remove('scanner-active');
  }

  private async startScanningWeb() {
    console.log('Start scanning web');

    if (!('BarcodeDetector' in window)) {
      console.log('Barcode Detector is not supported in the current browser context.');
    } else {
      console.log('Barcode Detector supported!');
      this.barcodeDetector = new BarcodeDetector({
        formats: ['upc_a'],
      });
    }
    const stream = await navigator.mediaDevices.getUserMedia({
      video: true,
      audio: false,
    });

    this.videoObject.nativeElement.srcObject = stream;

    setTimeout(() => {
      this.scanInterval = setInterval(this.detectBarcode, 200);
    }, 1000);
  }

  private async detectBarcode(): Promise<void> {
    this.barcodeDetector.detect(this.videoObject.nativeElement).then(async (codes: string | any[]) => {
      console.log('Detecting barcode web', codes);
      if (codes.length === 0) return;
      if (typeof codes === 'string') {
        console.log('barcodes', codes);
        this.scannedBarcode = codes;
        this.scannedProduct$ = this.http.getProductByBarcode(codes).pipe(
          catchError(() => {
            this.productNotFound = true;
            return of(undefined);
          })
        );
      }
    });
  }

  private stopScanningWeb() {
    console.log('Stop scanning web');
    clearInterval(this.scanInterval);
    this.videoObject.nativeElement.srcObject = null;
  }

  private simulateProductScannedSucceeded(event: any) {
    event.preventDefault();
    event.stopPropagation();

    console.log('Simulating product scanned succeeded...');
    this.initializeSearch();

    this.scannedBarcode = exampleBarcodes[Math.floor(Math.random() * exampleBarcodes.length)];
    this.scanResult = { hasContent: true, content: this.scannedBarcode };

    this.scannedProduct$ = this.http.getProductByBarcode(this.scanResult.content).pipe(
      catchError(() => {
        this.productNotFound = true;
        return of(undefined);
      })
    );
  }

  private simulateProductScannedFailed(event: any) {
    event.preventDefault();
    event.stopPropagation();

    console.log('Simulating product scanned failed...');
    this.initializeSearch();

    this.scannedBarcode = Math.floor(Math.random() * 9999999999999).toString();
    this.scanResult = { hasContent: true, content: this.scannedBarcode };

    this.scannedProduct$ = this.http.getProductByBarcode(this.scanResult.content).pipe(
      catchError(() => {
        this.productNotFound = true;
        return of(undefined);
      })
    );
  }
}
