import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { FeedbackButtonModule } from 'src/app/components/feedback-button/feedback-button.module';
import { SearchResultModule } from 'src/app/components/search-result/search-result.module';
import { SearchPageRoutingModule } from './search-routing.module';
import { SearchPage } from './search.page';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, SearchPageRoutingModule, FeedbackButtonModule, SearchResultModule],
  declarations: [SearchPage],
})
export class SearchPageModule {}
