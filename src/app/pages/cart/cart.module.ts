import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CartItemModule } from 'src/app/components/cart-item/cart-item.module';
import { FeedbackButtonModule } from 'src/app/components/feedback-button/feedback-button.module';
import { SavedCartModule } from 'src/app/components/saved-cart/saved-cart.module';
import { CartPageRoutingModule } from './cart-routing.module';
import { CartPage } from './cart.page';

@NgModule({
  declarations: [CartPage],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CartPageRoutingModule,
    FeedbackButtonModule,
    CartItemModule,
    SavedCartModule,
  ],
})
export class CartPageModule {}
