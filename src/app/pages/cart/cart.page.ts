import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { map } from 'rxjs/operators';
import { DeviceService } from 'src/app/services/device.service';
import { HttpService } from 'src/app/services/http.service';
import { LoadingService } from 'src/app/services/loading.service';
import { Cart, Product } from 'src/app/state/app.models';
import {
  addProductToCart,
  removeProductFromCart,
  setCurrentCart,
} from 'src/app/state/current-cart/current-cart.actions';
import { selectCurrentCart } from 'src/app/state/current-cart/current-cart.selectors';
import { saveCart } from 'src/app/state/saved-carts/saved-carts.actions';
import { selectSavedCarts } from 'src/app/state/saved-carts/saved-carts.selectors';

@Component({
  selector: 'hgv-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage {
  currentCart$ = this.store.select(selectCurrentCart);
  savedCarts$ = this.store.select(selectSavedCarts);
  totalScore$ = this.currentCart$.pipe(
    map((cart) => {
      const totalEcoScore = cart.items.reduce(
        (sum, next) => (sum += next.product.ecoscoreDto?.score * next.quantity),
        0
      );
      const totalQuantity = cart.items.reduce((sum, next) => (sum += next.quantity), 0);
      return Math.floor(totalEcoScore / totalQuantity) || 0;
    })
  );

  view = 'cart';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private store: Store,
    private http: HttpService,
    private loadingService: LoadingService,
    public device: DeviceService
  ) {
    const routeParams = this.route.snapshot.paramMap;
    const cartId = routeParams.get('cartId');

    if (cartId) {
      store.dispatch(setCurrentCart({ cartId }));
    }
  }

  removeFromCart(name: string) {
    this.store.dispatch(removeProductFromCart({ name }));
  }

  addToCart(product: Product) {
    this.store.dispatch(addProductToCart({ product }));
  }

  saveCart(cart: Cart) {
    this.store.dispatch(saveCart({ cart }));
    // TODO: create clear cart reducer
    // this.store.dispatch(clearCurrentCart())
    this.view = 'carts';
  }

  editCart(cart: Cart) {
    this.store.dispatch(setCurrentCart({ cartId: cart.id }));
    this.view = 'cart';
  }

  removeCart() {}
}
