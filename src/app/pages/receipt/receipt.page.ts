/* eslint-disable id-blacklist */
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Camera, CameraResultType, CameraSource } from '@capacitor/camera';
import { Store } from '@ngrx/store';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { DeviceService } from 'src/app/services/device.service';
import { HttpService } from 'src/app/services/http.service';
import { LoadingService } from 'src/app/services/loading.service';
import { Product, ProductSuggestion, RecognizedReceiptItem } from 'src/app/state/app.models';
import { addProductsToCart } from 'src/app/state/current-cart/current-cart.actions';
import * as Tesseract from 'tesseract.js';

@Component({
  selector: 'hgv-receipt',
  templateUrl: './receipt.page.html',
  styleUrls: ['./receipt.page.scss'],
})
export class ReceiptPage {
  step = 0;

  // receiptImageString = '/assets/images/testbonnetje.png';
  receiptImageString: string = undefined;
  croppedImageString: string = undefined;

  recognizeResult: Tesseract.RecognizeResult = undefined;
  recognizedText: RecognizedReceiptItem[] = undefined;

  productSuggestions: ProductSuggestion[] = undefined;

  constructor(
    private router: Router,
    private store: Store,
    private httpService: HttpService,
    private loadingService: LoadingService,
    public device: DeviceService
  ) {}

  init() {
    this.step = 0;
    this.receiptImageString = undefined;
    this.croppedImageString = undefined;
    this.recognizeResult = undefined;
    this.recognizedText = undefined;
    this.productSuggestions = undefined;
  }

  async onCaptureImage() {
    try {
      const capturedImage = await Camera.getPhoto({
        quality: 100,
        allowEditing: true,
        resultType: CameraResultType.DataUrl,
        source: CameraSource.Camera,
      });
      this.receiptImageString = capturedImage.dataUrl;
      this.step++;
    } catch (err) {
      console.error(err);
    }
  }

  onCropImage(event: ImageCroppedEvent) {
    this.croppedImageString = event.base64;
  }

  onDiscardImage() {
    this.receiptImageString = null;
    this.step--;
  }

  async onConfirmCrop() {
    await this.loadingService.present();
    try {
      this.recognizeResult = await Tesseract.recognize(this.croppedImageString, 'eng', {
        logger: (message) => console.log(message),
      });
    } catch (err) {
      this.step = -2;
      console.error(err);
    }

    if (this.recognizeResult) {
      this.recognizedText = this.getNamesAndPricesFromRecognizedText(this.recognizeResult);
      this.step++;
    } else {
      this.step = -1;
    }
    await this.loadingService.dismiss();
  }

  onDiscardRecognizedTextItem(name: string) {
    const index = this.recognizedText.findIndex((t) => t.name === name);
    this.recognizedText.splice(index, 1);
    if (!this.recognizedText.length) {
      this.step--;
    }
  }

  onDiscardRecognizedText() {
    this.recognizedText = [];
    this.step--;
  }

  async onConfirmRecognizedText() {
    const names = this.recognizedText.map((t) => t.name).join(',');
    const prices = this.recognizedText.map((t) => t.price).join(',');

    await this.loadingService.present();

    this.httpService.getProductSuggestionsByNamesAndPrices(names, prices).subscribe(
      async (suggestions) => {
        this.productSuggestions = suggestions.map((s, i) => ({
          ...s,
          receiptText: this.recognizedText[i].name ?? '',
          selectedProduct: s.bestGuess ?? s.allCandidates.shift(),
        }));
        this.step++;
        await this.loadingService.dismiss();
      },
      async (error) => {
        console.error(error);
        this.step = -2;
        await this.loadingService.dismiss();
      }
    );
  }

  onDiscardProductSuggestion(selectedProduct: Product) {
    const index = this.productSuggestions.findIndex((s) => s.selectedProduct === selectedProduct);
    if (this.productSuggestions[index].allCandidates) {
      this.productSuggestions[index].selectedProduct = this.productSuggestions[index].allCandidates.shift();
    } else {
      this.productSuggestions[index].selectedProduct = undefined;
    }
  }

  onDiscardProductSuggestions() {
    this.productSuggestions = [];
    this.step--;
  }

  onConfirmProductSuggestions() {
    const selectedProducts = this.productSuggestions
      .filter((s) => s.selectedProduct) // remove products without suggestion
      .map((s) => s.selectedProduct); // retrieve only the ProductDetails object
    this.store.dispatch(addProductsToCart({ products: selectedProducts }));
    this.productSuggestions = [];
    this.step++;
  }

  onGoToCart() {
    this.router.navigate(['cart']);
  }

  onGoToReceipt() {
    this.init();
  }

  private getNamesAndPricesFromRecognizedText(image: Tesseract.RecognizeResult): RecognizedReceiptItem[] {
    const namesAndPrices: RecognizedReceiptItem[] = [];

    image.data.text.split('\n').forEach((line) => {
      const nameAndPrice = line.split(/\s(\d+([., ])*\d+)$/);

      let name = nameAndPrice[0];
      if (name) {
        name = name
          .replace(/[^A-Za-z\s]/g, '') // remove anything but letters or whitespace
          .trim(); // remove trailing whitespace
      }

      let price = nameAndPrice[1];
      if (price) {
        price = price
          .replace(',', '.') // replace comma with dot for comma separation backend request.
          .replace(' ', '') // remove spaces from prices
          .replace(/[a-zA-Z]/, '0'); // remove letters from prices
      }

      if (name && price) {
        namesAndPrices.push({ name, price });
      }
    });

    return namesAndPrices;
  }
}
