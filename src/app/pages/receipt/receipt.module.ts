import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ImageCropperModule } from 'ngx-image-cropper';
import { FeedbackButtonModule } from 'src/app/components/feedback-button/feedback-button.module';
import { ReceiptPageRoutingModule } from './receipt-routing.module';
import { ReceiptPage } from './receipt.page';

@NgModule({
  declarations: [ReceiptPage],
  imports: [CommonModule, FormsModule, IonicModule, ReceiptPageRoutingModule, ImageCropperModule, FeedbackButtonModule],
})
export class ReceiptPageModule {}
