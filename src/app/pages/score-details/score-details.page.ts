/* eslint-disable id-blacklist */
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { DeviceService } from 'src/app/services/device.service';
import { HttpService } from 'src/app/services/http.service';
import { LoadingService } from 'src/app/services/loading.service';
import { Product } from 'src/app/state/app.models';

@Component({
  selector: 'hgv-score-details',
  templateUrl: './score-details.page.html',
  styleUrls: ['./score-details.page.scss'],
})
export class ScoreDetailsPage implements OnInit, OnDestroy {
  product$: Observable<Product>;
  result$: Observable<any>;
  result: Product;

  private subscription: Subscription;

  constructor(
    private route: ActivatedRoute,
    private httpService: HttpService,
    public device: DeviceService,
    private loadingService: LoadingService
  ) {}

  async ngOnInit(): Promise<void> {
    await this.loadingService.present();
    const routeParams = this.route.snapshot.paramMap;
    const barcode = routeParams.get('barcode');

    this.product$ = this.httpService.getProductByBarcode(barcode).pipe(
      tap(async () => {
        await this.loadingService.dismiss();
      })
    );
  }

  ngOnDestroy(): void {
    this.subscription?.unsubscribe();
  }
}
