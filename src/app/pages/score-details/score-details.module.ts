import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { FeedbackButtonModule } from 'src/app/components/feedback-button/feedback-button.module';
import { ScoreDetailsPageRoutingModule } from './score-details-routing.module';
import { ScoreDetailsPage } from './score-details.page';

@NgModule({
  declarations: [ScoreDetailsPage],
  imports: [CommonModule, FormsModule, IonicModule, ScoreDetailsPageRoutingModule, FeedbackButtonModule],
})
export class ScoreDetailsPageModule {}
