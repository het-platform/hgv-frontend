import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { from, Observable, of, Subscription } from 'rxjs';
import { catchError, concatAll, tap } from 'rxjs/operators';
import { DeviceService } from 'src/app/services/device.service';
import { HttpService } from 'src/app/services/http.service';
import { LoadingService } from 'src/app/services/loading.service';
import { Product } from 'src/app/state/app.models';

/**
 * This page handles showing the product submission page, with the possibility to submit the following data:
 * 1. a product barcode
 * 2. a product name
 * 3. four product images
 */
@Component({
  selector: 'hgv-submit',
  templateUrl: './submit.page.html',
  styleUrls: ['./submit.page.scss'],
})
export class SubmitPage implements OnInit, OnDestroy {
  product$: Observable<Product>;
  productNotFound = false;
  productName: string;
  frontImageUrl: string;
  frontImage: File;
  ingredientsImage: File;
  nutrientsImage: File;
  packagingImage: File;

  private subscription: Subscription;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private httpService: HttpService,
    private loadingService: LoadingService,
    public device: DeviceService
  ) {}

  async ngOnInit(): Promise<void> {
    await this.loadingService.present();
    const barcodeFromRoute = this.route.snapshot.paramMap.get('barcode');
    if (!barcodeFromRoute) {
      this.router.navigateByUrl('search');
      return;
    }
    this.product$ = this.httpService.getProductByBarcode(barcodeFromRoute).pipe(
      tap(async (product) => {
        this.productNotFound = false;
        this.productName = product.name;
        this.frontImageUrl = product.imageUrl;
        await this.loadingService.dismiss();
      }),
      catchError(async () => {
        this.productNotFound = true;
        await this.loadingService.dismiss();
        return { barcode: barcodeFromRoute };
      })
    );
  }

  submitProduct(barcode): void {
    const requestsToSend = [];
    if (this.productNotFound) {
      requestsToSend.push(this.httpService.postProductName(barcode, this.productName));
    }
    if (this.frontImage !== undefined && this.frontImage !== null) {
      requestsToSend.push(this.httpService.postProductImage(barcode, 'front', this.frontImage));
    }
    if (this.ingredientsImage !== undefined && this.ingredientsImage !== null) {
      requestsToSend.push(this.httpService.postProductImage(barcode, 'ingredients', this.ingredientsImage));
    }
    if (this.nutrientsImage !== undefined && this.nutrientsImage !== null) {
      requestsToSend.push(this.httpService.postProductImage(barcode, 'nutrients', this.nutrientsImage));
    }
    if (this.packagingImage !== undefined && this.packagingImage !== null) {
      requestsToSend.push(this.httpService.postProductImage(barcode, 'packaging', this.packagingImage));
    }
    requestsToSend.push(this.httpService.getProductByBarcode(barcode));

    from(requestsToSend)
      .pipe(concatAll())
      .subscribe((result) => {
        this.product$ = of(result);
      });
  }

  ngOnDestroy(): void {
    this.subscription?.unsubscribe();
  }
}
