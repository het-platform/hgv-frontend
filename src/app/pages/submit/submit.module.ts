import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SubmitPageRoutingModule } from './submit-routing.module';

import { FeedbackButtonModule } from 'src/app/components/feedback-button/feedback-button.module';
import { ImageUploadModule } from 'src/app/components/image-upload/image-upload.module';
import { SubmitPage } from './submit.page';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, SubmitPageRoutingModule, ImageUploadModule, FeedbackButtonModule],
  declarations: [SubmitPage],
})
export class SubmitPageModule {}
