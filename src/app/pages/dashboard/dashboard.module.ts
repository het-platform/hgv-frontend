import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { FeedbackButtonModule } from 'src/app/components/feedback-button/feedback-button.module';
import { DashboardPageRoutingModule } from './dashboard-routing.module';
import { DashboardPage } from './dashboard.page';

@NgModule({
  declarations: [DashboardPage],
  imports: [CommonModule, FormsModule, IonicModule, DashboardPageRoutingModule, FeedbackButtonModule],
})
export class DashboardPageModule {}
