import { Component } from '@angular/core';
import { DeviceService } from 'src/app/services/device.service';

@Component({
  selector: 'hgv-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage {
  constructor(public device: DeviceService) {}
}
