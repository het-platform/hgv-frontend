/* eslint-disable prefer-arrow/prefer-arrow-functions */
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { ActionReducer, StoreModule } from '@ngrx/store';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TabsComponent } from './components/tabs/tabs.component';
import { currentCartReducer } from './state/current-cart/current-cart.reducer';
import { savedCartsReducer } from './state/saved-carts/saved-carts.reducer';

const reducers = {
  currentCart: currentCartReducer,
  savedCarts: savedCartsReducer,
};

function debugReducer(reducer: ActionReducer<any>): ActionReducer<any> {
  return function (state, action) {
    return reducer(state, action);
  };
}

// TODO: make storage meta reducer
// function storageReducer(reducer: ActionReducer<any>): ActionReducer<any> {
//   return function (state, action) {
//     return reducer(state, action);
//   };
// }

export const metaReducers = { metaReducers: [debugReducer] };

@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    IonicModule.forRoot(),
    StoreModule.forRoot(reducers, metaReducers),
  ],
  providers: [
    {
      provide: RouteReuseStrategy,
      useClass: IonicRouteStrategy,
    },
  ],
  declarations: [AppComponent, TabsComponent],
  bootstrap: [AppComponent],
})
export class AppModule {}
