import { Cart } from './app.models';

export interface AppState {
  currentCart: Readonly<Cart>;
  savedCarts: ReadonlyArray<Cart>;
}
