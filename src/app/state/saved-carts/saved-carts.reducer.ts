import { createReducer, on } from '@ngrx/store';
import { Cart } from '../app.models';
import { deleteCart, getSavedCarts, saveCart } from './saved-carts.actions';

// TODO: get rid of readonly?
export const initialState: ReadonlyArray<Cart> = [];

export const savedCartsReducer = createReducer(
  initialState,
  on(getSavedCarts, (state) => {
    const storedCarts: Cart[] = JSON.parse(localStorage.getItem('savedCarts'));
    if (!storedCarts) {
      localStorage.setItem('savedCarts', JSON.stringify(state));
      return state;
    }
    return storedCarts;
  }),
  on(saveCart, (state, { cart }) => {
    let finalState = [...state, cart];
    const cartIndex = state.map((_cart) => _cart.id).indexOf(cart.id);
    if (cartIndex > -1) {
      finalState = state.map((s) => (s.id === cart.id ? cart : s));
    }
    localStorage.setItem('savedCarts', JSON.stringify(finalState));
    return finalState;
  }),
  on(deleteCart, (state, { cartId }) => {
    const finalState = state.filter((cart) => cart.id !== cartId);
    localStorage.setItem('savedCarts', JSON.stringify(finalState));
    return finalState;
  })
);
