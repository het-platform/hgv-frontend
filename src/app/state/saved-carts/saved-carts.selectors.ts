import { createFeatureSelector } from '@ngrx/store';
import { Cart } from '../app.models';

export const selectSavedCarts = createFeatureSelector<ReadonlyArray<Cart>>('savedCarts');
