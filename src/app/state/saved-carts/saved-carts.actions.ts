import { createAction, props } from '@ngrx/store';
import { Cart } from '../app.models';

export const getSavedCarts = createAction('[Current Cart] Get Current Cart');

export const saveCart = createAction('[Saved Carts] Save Cart', props<{ cart: Cart }>());

export const deleteCart = createAction('[Saved Carts] Delete Cart', props<{ cartId: string }>());
