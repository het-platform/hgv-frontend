import { createReducer, on } from '@ngrx/store';
import { Cart } from '../app.models';
import {
  addProductsToCart,
  addProductToCart,
  getCurrentCart,
  removeProductFromCart,
  setCurrentCart,
} from './current-cart.actions';

const getFormattedDate = () => {
  const date = new Date(Date.now());
  return `${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}`;
};

const getRandomNumber = () => Math.floor(Math.random() * 99999).toString();

const getUniqueId = () => {
  const savedCarts: Cart[] = JSON.parse(localStorage.getItem('savedCarts'));
  if (!savedCarts) return getRandomNumber();
  const usedIds = savedCarts.map((c) => c.id);
  let newId = getRandomNumber();
  while (usedIds.includes(newId)) {
    newId = getRandomNumber();
  }
  return newId;
};

export const initialState: Readonly<Cart> = {
  id: getUniqueId(),
  title: 'Mijn winkelmand',
  items: [],
  date: getFormattedDate(),
};

// TODO: create storage meta reducer
export const currentCartReducer = createReducer(
  initialState,
  on(getCurrentCart, (state) => {
    const storedCart: Cart = JSON.parse(localStorage.getItem('currentCart'));
    if (!storedCart) {
      localStorage.setItem('currentCart', JSON.stringify(state));
      return state;
    }
    return storedCart;
  }),
  on(setCurrentCart, (_, { cartId }) => {
    const storedCarts: Cart[] = JSON.parse(localStorage.getItem('savedCarts'));
    const newCart = storedCarts.find((cart) => cart.id === cartId);
    localStorage.setItem('currentCart', JSON.stringify(newCart));
    return newCart;
  }),
  on(addProductToCart, (state, { product }) => {
    let finalState = { ...state, items: [...state.items, { product, quantity: 1 }] };
    const cartItemIndex = state.items.map((i) => i.product.name).indexOf(product.name);
    if (cartItemIndex > -1) {
      finalState = {
        ...state,
        items: state.items.map((c, i) => (i === cartItemIndex ? { ...c, quantity: c.quantity + 1 } : c)),
      };
    }
    localStorage.setItem('currentCart', JSON.stringify(finalState));
    return finalState;
  }),
  on(addProductsToCart, (state, { products }) => {
    const finalState = { ...state };
    for (const product of products) {
      const productIndex = state.items.map((i) => i.product.name).indexOf(product.name);
      if (productIndex > -1) {
        finalState.items[productIndex].quantity += 1;
      } else {
        finalState.items.push({ product, quantity: 1 });
      }
    }
    localStorage.setItem('currentCart', JSON.stringify(finalState));
    return finalState;
  }),
  on(removeProductFromCart, (state, { name }) => {
    const cartItemIndex = state.items.map((i) => i.product.name).indexOf(name);
    const finalState = {
      ...state,
      items: state.items
        .map((c, i) => (i === cartItemIndex ? { ...c, quantity: c.quantity - 1 } : c))
        .filter((c) => c.quantity > 0),
    };
    localStorage.setItem('currentCart', JSON.stringify(finalState));
    return finalState;
  })
);
