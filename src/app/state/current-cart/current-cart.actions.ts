import { createAction, props } from '@ngrx/store';
import { Product } from '../app.models';

export const getCurrentCart = createAction('[Current Cart] Get Current Cart');

export const setCurrentCart = createAction('[Current Cart] Set Current Cart', props<{ cartId: string }>());

export const addProductToCart = createAction('[Current Cart] Add Product To Cart', props<{ product: Product }>());

export const addProductsToCart = createAction('[Current Cart] Add Products To Cart', props<{ products: Product[] }>());

export const removeProductFromCart = createAction('[Current Cart] Remove Product From Cart', props<{ name: string }>());
