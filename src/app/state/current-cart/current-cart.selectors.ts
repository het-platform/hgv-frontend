import { createFeatureSelector } from '@ngrx/store';
import { Cart } from '../app.models';

export const selectCurrentCart = createFeatureSelector<Readonly<Cart>>('currentCart');

// export const selectBookCollection = createSelector(selectBooks, selectCollectionState, (books, collection) =>
//   collection.map((id) => books.find((book) => book.id === id))
// );
