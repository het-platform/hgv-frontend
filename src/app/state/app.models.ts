export interface Cart {
  id: string;
  title: string;
  date: string;
  items: CartItem[];
}

export interface CartItem {
  product: Product;
  quantity: number;
}

export interface Product {
  barcode?: string;
  ecoscoreDto?: EcoScore;
  imageUrl?: string;
  name?: string;
  detailsLink?: string;
  ecoscoreLink?: string;
}

export interface EcoScore {
  co2EquivalentPerKg: number;
  co2ImpactDistribution: ImpactDistribution;
  environmentalFootprintDistribution: ImpactDistribution;
  grade: string;
  score: number;
  source: string;
  standardDeviation: any;
}

export interface ImpactDistribution {
  agriculture: number;
  consumption: number;
  distribution: number;
  packaging: number;
  processing: number;
  transportation: number;
}

export interface RecognizedReceiptItem {
  name: string;
  price: string;
}

export interface ProductSuggestion {
  allCandidates?: Product[];
  bestGuess?: Product;
  selectedProduct?: Product;
  receiptText?: string;
}

export interface SubmittedProduct {
  name: string;
  frontImage: string;
  ingredientsImage: string;
  nutrientsImage: string;
  packagingImage: string;
}

export type PhotoType = 'front' | 'ingredients' | 'nutrients' | 'packaging';
