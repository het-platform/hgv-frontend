import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { TabsComponent } from './components/tabs/tabs.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: '',
    component: TabsComponent,
    children: [
      {
        path: 'score-details/:barcode',
        loadChildren: () => import('./pages/score-details/score-details.module').then((m) => m.ScoreDetailsPageModule),
      },
      {
        path: 'receipt',
        loadChildren: () => import('./pages/receipt/receipt.module').then((m) => m.ReceiptPageModule),
      },
      {
        path: 'dashboard',
        loadChildren: () => import('./pages/dashboard/dashboard.module').then((m) => m.DashboardPageModule),
      },
      {
        path: 'search',
        loadChildren: () => import('./pages/search/search.module').then((m) => m.SearchPageModule),
      },
      {
        path: 'cart',
        loadChildren: () => import('./pages/cart/cart.module').then((m) => m.CartPageModule),
      },
      {
        path: 'account',
        loadChildren: () => import('./pages/account/account.module').then((m) => m.AccountPageModule),
      },
      {
        path: 'submit/:barcode',
        loadChildren: () => import('./pages/submit/submit.module').then((m) => m.SubmitPageModule),
      },
    ],
  },
];
@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
