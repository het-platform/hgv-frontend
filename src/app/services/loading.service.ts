import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root',
})
export class LoadingService {
  private loader: any;

  constructor(private loadingController: LoadingController) {}

  async present(): Promise<void> {
    this.loader = await this.loadingController.create({ message: 'Loading...' });
    this.loader.present();
  }

  async dismiss() {
    this.loader.dismiss();
  }
}
