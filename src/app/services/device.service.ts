import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';

@Injectable({
  providedIn: 'root',
})
export class DeviceService {
  constructor(private platform: Platform) {
    console.log(platform.platforms());
  }

  get isIphone() {
    return (
      this.platform.is('iphone') &&
      this.platform.is('ios') &&
      this.platform.is('cordova') &&
      this.platform.is('capacitor') &&
      this.platform.is('mobile') &&
      this.platform.is('hybrid')
    );
  }

  get isAndroid() {
    // TODO: implement
    return false;
  }

  get isMobileWeb() {
    // TODO: include more platforms
    return this.platform.is('mobileweb');
  }
}
