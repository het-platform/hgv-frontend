/* eslint-disable @typescript-eslint/naming-convention */
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EMPTY, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { exampleBarcodes } from '../constants';
import { EcoScore, PhotoType, Product, ProductSuggestion } from '../state/app.models';

@Injectable({
  providedIn: 'root',
})
export class HttpService {
  useOffTestServer = environment.production ? 'false' : 'true';
  // useOffTestServer = 'true';

  options = {
    headers: {
      'X-UseOffTestServer': this.useOffTestServer,
    },
  };

  constructor(private httpClient: HttpClient) {}

  getProductByBarcode(barcode: string): Observable<Product> {
    const url = `${environment.backendUrl}/products?barcode=${barcode}`;
    return this.httpClient.get<Product>(url, this.options).pipe(map(this.mapEcoscoreDataToPercentages));
  }

  getProductsByName(name: string): Observable<Product[]> {
    const url = `${environment.backendUrl}/products/search?name=${name}`;
    return this.httpClient.get<Product[]>(url, this.options);
  }

  getProductSuggestionsByNamesAndPrices(names: string, prices: string): Observable<ProductSuggestion[]> {
    const url = `${environment.backendUrl}/receipts/ah/search?names=${names}&prices=${prices}`;
    return this.httpClient.get<ProductSuggestion[]>(url, this.options);
  }

  getProductDetailsLink(link: string): Observable<Product> {
    const url = `${environment.backendUrl}${link}`;
    return this.httpClient.get<Product>(url, this.options);
  }

  getEcoscoreDetailsLink(link: string): Observable<EcoScore> {
    const url = `${environment.backendUrl}${link}`;
    return this.httpClient.get<EcoScore>(url, this.options);
  }

  getRandomProductByBarcode(): Observable<Product> {
    const barcode = exampleBarcodes[Math.round(Math.random() * (exampleBarcodes.length - 1))];
    const url = `${environment.backendUrl}/products?barcode=${barcode}`;
    return this.httpClient.get<Product>(url, this.options);
  }

  postProductName(barcode: string, productName: string) {
    const url = `${environment.backendUrl}/products/${barcode}?name=${productName}`;
    return this.httpClient.post(url, null, {
      ...this.options.headers,
      responseType: 'text',
    });
  }

  postProductImage(barcode: string, photoType: PhotoType, imageInputFile: File) {
    if (!imageInputFile) return EMPTY;
    const url = `${environment.backendUrl}/products/${barcode}/photos?photo_type=${photoType}`;
    const formData = new FormData();
    formData.append('photo', imageInputFile);
    return this.httpClient.post<Product>(url, formData, {
      ...this.options.headers,
      responseType: 'text' as 'json',
    });
  }

  private mapEcoscoreDataToPercentages(product: Product): Product {
    const finalProduct = { ...product };
    if (product.ecoscoreDto?.environmentalFootprintDistribution) {
      finalProduct.ecoscoreDto.environmentalFootprintDistribution = {
        agriculture: parseFloat(product.ecoscoreDto.environmentalFootprintDistribution.agriculture.toFixed(1)),
        consumption: parseFloat(product.ecoscoreDto.environmentalFootprintDistribution.consumption.toFixed(0)),
        distribution: parseFloat(product.ecoscoreDto.environmentalFootprintDistribution.distribution.toFixed(0)),
        packaging: parseFloat(product.ecoscoreDto.environmentalFootprintDistribution.packaging.toFixed(0)),
        processing: parseFloat(product.ecoscoreDto.environmentalFootprintDistribution.processing.toFixed(0)),
        transportation: parseFloat(product.ecoscoreDto.environmentalFootprintDistribution.transportation.toFixed(0)),
      };
    }
    if (product.ecoscoreDto?.co2ImpactDistribution) {
      finalProduct.ecoscoreDto.co2ImpactDistribution = {
        agriculture: parseFloat(product.ecoscoreDto.co2ImpactDistribution.agriculture.toFixed(1)),
        consumption: parseFloat(product.ecoscoreDto.co2ImpactDistribution.consumption.toFixed(0)),
        distribution: parseFloat(product.ecoscoreDto.co2ImpactDistribution.distribution.toFixed(0)),
        packaging: parseFloat(product.ecoscoreDto.co2ImpactDistribution.packaging.toFixed(0)),
        processing: parseFloat(product.ecoscoreDto.co2ImpactDistribution.processing.toFixed(0)),
        transportation: parseFloat(product.ecoscoreDto.co2ImpactDistribution.transportation.toFixed(0)),
      };
    }
    return finalProduct;
  }
}
